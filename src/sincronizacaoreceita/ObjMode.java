package sincronizacaoreceita;

/**
 * @author gleidiomar freitas
 */
public class ObjMode {

    public String  agencia;
    public String  conta;
    public double  saldo;
    public char    status;
    public boolean comentario;

    public ObjMode() {
    }

    public ObjMode(String agencia, String conta, double saldo, char status) {
        this.agencia = agencia;
        this.conta = conta;
        this.saldo = saldo;
        this.status = status;
    }
}
