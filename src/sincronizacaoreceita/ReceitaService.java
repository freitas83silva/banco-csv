package sincronizacaoreceita;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.StringJoiner;
import java.util.stream.Stream;

/**
 * @author gleidiomar freitas
 */
public class ReceitaService {

	String local = "src/sincronizacaoreceita/";
	
    Path localOri = Paths.get(local + "local_origem.csv");
    Path localDes = Paths.get(local + "local_destino.csv");
    
	// Esta é a implementação interna do "servico" do banco central. Veja o código fonte abaixo para ver os formatos esperados pelo Banco Central neste cenário.

    public boolean atualizarConta(String agencia, String conta, double saldo, String status)
            throws RuntimeException, InterruptedException {
		
			
        // Formato agencia: 0000
        if (agencia == null || agencia.length() != 4) {
        	System.out.println("Agencia False");
            return false;
        }
        
        // Formato conta: 000000
        if (conta == null || conta.length() != 6) {
        	System.out.println("Conta False");
            return false;
        }
        
        // Tipos de status validos:
        List tipos = new ArrayList();
        tipos.add("A");
        tipos.add("I");
        tipos.add("B");
        tipos.add("P");                
                
        if (status == null || !tipos.contains(status)) {
        	System.out.println("Tipo False");
            return false;
        }

        // Simula tempo de resposta do serviço (entre 1 e 5 segundos)
        long wait = Math.round(Math.random() * 4000) + 1000;
        Thread.sleep(wait);

        // Simula cenario de erro no serviço (0,1% de erro)
        long randomError = Math.round(Math.random() * 1000);
        if (randomError == 500) {
            throw new RuntimeException("Error");
        }

        List<ObjMode> objetos = openFileCSV(localOri);
        
        String conteudo = converterObjsString(objetos, agencia, conta, saldo, status);
        
        writeCSV(conteudo, localOri);
        
        System.out.println("Ok");
        return true;
    }
    
    private List<ObjMode> openFileCSV(Path file) {
        List<ObjMode> objs = new ArrayList<>();

        try {
            Files.readAllLines(file)
                 .stream()
                 .skip(1)
                 .map(this::newObjoCSV)
                 .filter(Objects::nonNull)
                 .forEach(objs::add);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return objs;
    }
    
    private ObjMode newObjoCSV(String linha) {
        
    	String[] valores = linha.split(";");

        if (valores.length != 4) return null;

        ObjMode obj = new ObjMode();
        obj.agencia = valores[0];
        obj.conta = valores[1];
        obj.status = valores[3].charAt(0);

        Scanner scanner = new Scanner(valores[2]);
        obj.saldo = scanner.nextDouble();

        return obj;
    }
    
    private String converterObjsString(List<ObjMode> objetos, String agencia, String conta, double saldo, String status) {
    	
        NumberFormat  formatadorDecimal = NumberFormat.getInstance();
        StringBuilder conteudo          = new StringBuilder();

        StringJoiner cabecalho = new StringJoiner(";", "", "");

        Stream.of(ObjMode.class.getDeclaredFields())
              .map(Field::getName)
              .forEach(cabecalho::add);

        conteudo.append(cabecalho)
                .append(System.lineSeparator());

        objetos.forEach(objeto -> conteudo.append(objeto.agencia)
                                          .append(";")
                                          .append(objeto.conta)
                                          .append(";")
                                          .append(formatadorDecimal.format(objeto.saldo))
                                          .append(";")
                                          .append(objeto.status)
                                          .append(";")
                                          .append(objeto.comentario)
                                          .append(System.lineSeparator()));

        conteudo.append(agencia)
		        .append(";")
		        .append(conta)
		        .append(";")
		        .append(formatadorDecimal.format(saldo))
		        .append(";")
		        .append(status)
		        .append(";")
		        .append("false")
		        .append(System.lineSeparator());
        
        return conteudo.toString();
    }
    
    private void writeCSV(String conteudo, Path arquivo) {
        try {
        	
        	PrintWriter writer = new PrintWriter(new File("src/sincronizacaoreceita/local_destino.csv"));
           
            Files.deleteIfExists(arquivo);

            System.out.println("Deletado Arquivo");
            
            writer.write(conteudo.toString());
            writer.close();
            System.out.println("done!");

            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
